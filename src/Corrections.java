import java.util.*;

public class Corrections {

    static String entry1;
    static String entry2;
    static String entry3;
    static String entry4;
    static String entry5;

    enum MyHeroes {
        Spiderman, CaptainAmerica, Hulk, Hercule, StupendousMan
    }

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Please enter five superheroes, starting with your most favorite.");
        entry1 = userInput.nextLine();
        entry2 = userInput.nextLine();
        entry3 = userInput.nextLine();
        entry4 = userInput.nextLine();
        entry5 = userInput.nextLine();


        List list = new ArrayList();
        list.add(entry1);
        list.add(entry2);
        list.add(entry3);
        list.add(entry4);
        list.add(entry5);

        System.out.println("Here's an array list of your heroes ordered from most to least favorite.");

        for (Object heroes : list) {
            System.out.println((String) heroes);
        }

        System.out.println("Now let's see an enumset of MY favorite superheroes.");
        Set<MyHeroes> allFaves = EnumSet.allOf(MyHeroes.class); //all of the enumset. could enter noneof, but why?
        System.out.println("Heroes:" + allFaves);
        System.out.println("Eh, don't like Hercule that much.");
        allFaves.remove(MyHeroes.Hercule);
        System.out.println("Heroes:" + allFaves);
        System.out.println("Let's remove them all.");
        allFaves.removeAll(allFaves);
        System.out.println("Heroes:" + allFaves);
        System.out.println("Eh, let's get them all back.");
        Set<MyHeroes> reFaves = EnumSet.allOf(MyHeroes.class);
        allFaves.addAll(reFaves);
        System.out.println("Heroes:" + allFaves);

        Object[] myArray = allFaves.toArray();
        System.out.println("And let's turn that into an array.");
        for (int i = 0; i < myArray.length; i++)
            System.out.println(myArray[i]);

        System.out.println("Now back to your list. This is a queue. ");
        Queue<String> wq = new LinkedList<>();
        wq.add(entry1);
        wq.add(entry2);
        wq.add(entry3);
        wq.add(entry4);
        wq.add(entry5);
        System.out.println(wq);

        System.out.println("Let's move " + entry2 + " and push your first choice to the back.");
        wq.poll();
        System.out.println("peek() will let us check who's first in the queue.");
        System.out.println(wq.peek());
        wq.add(entry1);
        System.out.println("element() works the same as peek, but can throw an error, but I don't care" +
                " about that right now.");
        System.out.println(wq.element());
        System.out.println(wq);
        System.out.println("And there you have it for queueueueueueueues.");

        //one more thing, linkedlist
        System.out.println("Linked List messed up.");
        List<OtherList> olist = new LinkedList<OtherList>();
        olist.add(new OtherList(entry1));
        olist.add(new OtherList(entry2));
        olist.add(new OtherList(entry4));
        olist.add(new OtherList(entry4));
        olist.add(new OtherList(entry5));

        for (OtherList ol : olist) {
            System.out.println(ol);
        }
        //Oh no, forgot entry3! and put in entry4 twice! what to do...
        System.out.println("Linked List corrected.");
        olist.remove(2);
        olist.add(2, new OtherList(entry3));

        for (OtherList ol : olist) {
            System.out.println(ol);
        }
    }

}
