public class OtherList {

    private String heroku;

    public OtherList(String heroku) {
        this.heroku = heroku;
    }

    public String toString() {
        return "Hero: " + heroku;
    }

}
